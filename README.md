This is a template repository for creating new repositories for upm (Unity Package Manager) packages.

Generated repository will contain:

Basis for Unity package (package.json, folder structure, assembly definitions etc.)
Basis for Runtime & Editor tests (can be run straight away after script has ran and package is in some Unity projects assets folder / marked as testable)
Sample/ExampleUnityProject - empty Unity project for running automated github tests via unity-ci
Note: Better way for doing this would be appreciated, there is an issue about this in unity-test-runner
package.json to publish into npm
Automated github action workflows to run tests on push (further documentation can be found in templates/.github/workflows)
Automated github action workflow to publish into npmjs on publish release
Note: If your package contains a lot of images/data or size exceeds 10mbs it might better to just publish into upm package manager / other service that is meant for storing assets and other big data. It is a good practice to check npm registry guidelines before usage.
Example package.json
Example README.md
Example CONTRIBUTING.md, based on Contributor Covenant
Example License (MIT)
Example CODEOWNERS, for further details please see github docs on CODEOWNERS
Note: By default this repository is meant for creating open source packages. If creating closed source it might be a good idea to change license and go through generated package.json files etc.

Usage
Create a new repository using this template
Clone the new repository
Run RUNME.sh (cautiously!) with bash at new repository root folder and follow instructions, if using windows Git for Windows provides bash and all necessary tools.
Move all files (expect folders .git/ .github/ and Samples/) inside any Unity Project Assets folder. This will generate .meta files that are required by Unity.
Move files back to the original folder
Notes:

RUNME.sh just helps replacing all tags etc. {{REPOSITORY_NAME}} or {{DESCRIPTION}} from files, if not familiar with bash scripts this step should be done manually
Brief introduction on how to develop your Unity package is included in CONTRIBUTING.md
For further details, please see

Github docs on creating repository from template
Unity docs on creating custom packages.
When your package is ready for publish, you can publish it into:

Open Source Unity Package Registry openupm
npmjs package registry
Note: If your package contains a lot of images/data or size exceeds 10mbs it might not be a good idea.
License
MIT License

Copyright © 2021 Gameready
